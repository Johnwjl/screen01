// 引入等比适配插件
const px2rem = require("postcss-px2rem");

// 配置基本大小
const postcss = px2rem({
  // 基准大小 baseSize，需要和rem.js中相同
  remUnit: 100,
});

module.exports = {
  lintOnSave: false,
  css: {
    loaderOptions: {
      postcss: {
        plugins: [postcss],
      },
    },
  },
  publicPath: "./", // 可以设置成相对路径，这样所有的资源都会被链接为相对路径，打出来的包可以被部署在任意路径
  outputDir: process.env.outputDir, // 打包时生成的生产环境构建文件的目录
  assetsDir: "public", // 放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录

  devServer: {
    host: "0.0.0.0",
    port: "8080", // 端口号
    https: false, // https: {type:Bollean}
    open: false, // 配置自动启动浏览器
    // 配置代理
    proxy: {
      "/api": {
        target: process.env.VUE_APP_API_URL, // 生产地址
        changeOrigin: true, // 开启跨域,在本地创建一个虚拟服务,然后发送请求的数据,并同时接收请求的数据,这样服务端和服务端进行数据交互就不会有问题
        pathRewrite: {
          "^/api": "", // 利用这个地面的值拼接上target里面的地址
        },
      },
    },
  },
  productionSourceMap: false, // 生产打包时不输出map文件，增加打包速度
  // 配置 发布环境取消console.log
  configureWebpack: (config) => {
    if (process.env.NODE_ENV === "production") {
      config.optimization.minimizer[0].options.terserOptions.compress.warnings = false;
      config.optimization.minimizer[0].options.terserOptions.compress.drop_console = true;
      config.optimization.minimizer[0].options.terserOptions.compress.drop_debugger = true;
      config.optimization.minimizer[0].options.terserOptions.compress.pure_funcs =
        ["console.log"];
      config.optimization.minimizer[0].options.output = {
        comments: false,
      };
    }
  },
};
