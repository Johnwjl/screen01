import { createStore } from 'vuex'

export default createStore({
  state: {
    // mapCenter: null, //加载地图中心点数据
    row: null, // 行数据
    clickNum: 0 // 点击次数
  },
  mutations: {
    // MAP_CENTER: (state, arr) => {
    //   state.mapCenter = arr
    // },
    MAP_ROW: (state, data) => {
      state.row = data
    },
    MAP_CLICKROW: (state, data) => {
      state.clickNum++
    }
  },
  actions: {
    // mapCenterData(context, arr) {
    //   context.commit("MAP_CENTER", arr);
    // },
    mapRowData (context, data) {
      context.commit('MAP_ROW', data)
    },
    clickRow (context, data) {
      context.commit('MAP_CLICKROW', data)
    }
  },
  modules: {}
})
