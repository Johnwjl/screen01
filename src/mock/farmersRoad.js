// 路段 测试数据
import { roadData1 } from '../mock/farmersRoad/roadData1'
// import { roadData2 } from '../mock/farmersRoad/roadData2'

const renderRoadData = roadData1
// const renderRoadData = [...roadData1, ...roadData2]

// 测试数据
export const farmersRoadData = [
  {
    name: '四川金川县',
    center: [102.046663, 31.6373],
    mileage: 6013.86, // 检测里程
    devicesNum: 8, // 投入设备数
    Days: 15, // 检测天数
    roadData: renderRoadData
  },
  {
    name: '黔南州',
    center: [107.522303, 26.253136],
    mileage: 6842.04, // 检测里程
    devicesNum: 8, // 投入设备数
    Days: 21 // 检测天数
  },
  {
    name: '黔东南州',
    center: [107.982838, 26.583759],
    mileage: 11682, // 检测里程
    devicesNum: 10, // 投入设备数
    Days: 30 // 检测天数
  },
  {
    name: '黔西南州',
    center: [104.906419, 25.087733],
    mileage: 7217.15, // 检测里程
    devicesNum: 10, // 投入设备数
    Days: 17 // 检测天数
  },
  {
    name: '六盘水市',
    center: [104.830357, 26.592538],
    mileage: 6770.08, // 检测里程
    devicesNum: 8, // 投入设备数
    Days: 21 // 检测天数
  },
  {
    name: '毕节市',
    center: [105.291544, 27.283615],
    mileage: 12331.6, // 检测里程
    devicesNum: 10, // 投入设备数
    Days: 28 // 检测天数
  },
  {
    name: '铜仁市',
    center: [109.189528, 27.731555],
    mileage: 8372.56, // 检测里程
    devicesNum: 12, // 投入设备数
    Days: 17 // 检测天数
  },
  {
    name: '汕头市',
    center: [116.681956, 23.354152],
    mileage: 8006, // 检测里程
    devicesNum: 10, // 投入设备数
    Days: 20 // 检测天数
  },
  {
    name: '宜昌市',
    center: [111.286962, 30.69217],
    mileage: 3225.8, // 检测里程
    devicesNum: 6, // 投入设备数
    Days: 13 // 检测天数
  },
  {
    name: '恩施市',
    center: [109.488076, 30.272104],
    mileage: 3603, // 检测里程
    devicesNum: 8, // 投入设备数
    Days: 11 // 检测天数
  },
  {
    name: '福清市',
    center: [119.384388, 25.720081],
    mileage: 4402.08, // 检测里程
    devicesNum: 8, // 投入设备数
    Days: 14 // 检测天数
  },
  {
    name: '湖州市南浔区',
    center: [120.418244, 30.850835],
    mileage: 355.1, // 检测里程
    devicesNum: 2, // 投入设备数
    Days: 5 // 检测天数
  },
  {
    name: '重庆市忠县',
    center: [108.038073, 30.299817],
    mileage: 1823.78, // 检测里程
    devicesNum: 4, // 投入设备数
    Days: 11 // 检测天数
  },
  {
    name: '嘉兴市嘉善县',
    center: [120.926031, 30.83085],
    mileage: 663,
    devicesNum: 2,
    Days: 10
  }
]
