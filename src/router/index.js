import { createRouter, createWebHashHistory } from 'vue-router'
import { getToken } from '@/utils/auth'
const routes = [
  {
    path: '/roadDisease',
    name: 'RoadDisease',
    component: () => import('../views/roadDisease/index.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/',
    redirect: '/login'
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
  if (to.path === '/login') {
    // 1. 如果跳转登录页面  无条件放行
    next()
  } else {
    // 2. 如果不是登录页  就要验证token了 （有token 就放行 重定向到首页了 否则就强制去登录页面）
    const tokenStr = getToken()
    if (tokenStr && tokenStr !== '') {
      next()
    } else {
      next('/login')
    }
  }
})

export default router
