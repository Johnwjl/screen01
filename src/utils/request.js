import axios from 'axios'
import { ElMessage } from 'element-plus'
import { getToken } from '@/utils/auth'

const service = axios.create({
  // baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
})

service.interceptors.request.use(
  (config) => {
    const token = getToken()
    if (token) {
      config.headers.token = token
    }

    config.headers['content-type'] = 'application/json;charset=UTF-8'
    return config
  },
  (error) => {
    if (error.response.status === 404) {
      ElMessage({
        message: '未找到资源，请联系管理员',
        type: 'error',
        duration: 5 * 1000
      })
    } else if (error.response.status === 500) {
      ElMessage({
        message: '服务器内部异常，请联系管理员',
        type: 'error',
        duration: 5 * 1000
      })
    }
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  (response) => {
    const res = response.data
    if (res.code !== 0) {
      ElMessage({
        message: res.msg || 'Error',
        type: 'error',
        duration: 5 * 1000
      })
    } else {
      return res
    }
  },
  (error) => {
    if (error.response.status === 404) {
      ElMessage({
        message: '未找到资源，请联系管理员',
        type: 'error',
        duration: 5 * 1000
      })
    } else if (error.response.status === 500) {
      ElMessage({
        message: '服务器内部异常，请联系管理员',
        type: 'error',
        duration: 5 * 1000
      })
    }
    return Promise.reject(error)
  }
)

export default service
