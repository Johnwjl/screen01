import service from '@/utils/request'
import axios from 'axios'
import qs from 'qs'
import { getToken } from '@/utils/auth'
// 登录
export const userLogin = (params) =>
  service.post(
    '/api/tlcloud/larkdata/sysmanager/login/larkdata/sysmanager/login',
    params
  )
// 退出
export const userLoginOut = (params) =>
  service.post(
    '/api/tlcloud/larkdata/sysmanager/login/larkdata/sysmanager/logout',
    params
  )

/* -----路面病害模块(右侧功能)----- */
// 道路列表
export const roadListApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getRoadListsByDmCompanyIds'
  )
// 道路画线列表
export const roadNetworkListApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getCommonRoadMarking'
  )
// 路段列表
export const sectionListApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryRoadLists',
    params
  )
// 设备列表
export const deviceListApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getDeviceListsByDmCompanyIds'
  )
// 单位列表
export const companyListApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getDmCompanyListsByDmCompanyIds'
  )
// 病害类型列表
export const diseaseTypeListApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getConfigDisease'
  )
// 病害列表
export const diseaseTableDataListApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getDiseaseListPage',
    params
  )
// 病害撒点
export const diseaseMapDataApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getDiseaseMapShow',
    params
  )
// 病害查询(详情)
export const diseaseDataInfoApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getDiseaseDetails',
    params
  )
// 电子病历(时光机)
export const diseaseTimeDataApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getTimeMachine',
    params
  )
/* -----路面病害模块(左侧功能)----- */
// 里程统计
export const mileageStatisticApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getMileageStatistics'
  )
// 已派单病害列表
export const sendOrderListApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/querySendOrderDiseaseList'
  )
// 每日病害数量
export const everyDayNumberApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryDailyNumberDiseases'
  )
// 时间轴选中病害统计
export const currentTimeLineDateApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getSelectTimeDiseaseStatistics',
    params
  )
/* -----地图时间轴----- */
export const timeLineListApi = () =>
  service.post('/api/tlcloud/larkdata/visualcity/larkdata/visualcity/timeline')

/* -----路下病害(左侧)----- */
// 路下病害table
export const underRoadDiseaseTable = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryRoadunderDisease',
    params
  )
// 路下病害地图撒点
export const underRoadDiseaseMap = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryDiseasePointList',
    params
  )
// 路下病害详情查询
export const underRoadDiseaseInfoApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryRoadunderDiseaseDetail',
    params
  )

/* -----路面舒适度----- */
// 标识状态列表
export const identityStatusListApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryAbnormalJumpTypeList'
  )
// 异常跳车点table列表
export const jumpCarPointListApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryAbnormalJumpsList',
    params
  )
// 异常跳车点地图撒点
export const jumpCarPointMapApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryLnglatList',
    params
  )
// 路段rqi数据
export const roadRQIDataApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryRoadRqiList'
  )
// 异常跳车点详情查询
export const jumpCarPointInfoApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryAbnormalJumpsInfo',
    params
  )
// 异常跳车点标记
export const jumpCarPointUpdate = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/markAbnormalJumps',
    params
  )
// 百米段跳车点数
export const jumpCarEchartApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryAbnormalJumpsCount'
  )
// 综合指数
export const compositeEchartApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryRoadComfortRank'
  )
// 异常跳车点导出(不进入请求拦截)
export const exportAbnormalPointDataApi = (params) =>
  axios.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/exportAbnormalJumpsList',
    params,
    { responseType: 'blob', headers: { token: getToken() } }
  )

/* -----巡查管理----- */
// 巡查道路
export const roadPatrolListApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getLeftRoadPatrolStatistics',
    params
  )
// 巡查道路详情查看
export const roadPatrolInfoImageListApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getLeftRoadPatrolStatisticsInfo',
    params
  )
// 巡查车辆补点
export const roadPointListApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getTlcVisualcityPatrolManageSubGps',
    params
  )
// 巡查车辆数据
export const deviceTableListApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getVehicleMileageStatisticsByDeviceCode',
    params
  )
// 设备巡查的道路id
export const devicePatrolRoadApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getDmRoadIdByDeviceCode',
    params
  )
// 设备巡查点的时光机
export const devicePatrolPointTimeMachineApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getRoadTangentGpsTimeMachine',
    params
  )
// 巡查周期查询
export const patrolCycleApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/getRoutePatrollingCycle',
    params
  )

/* -----统计分析 病害列表----- */
// 病害table数据列表
export const diseaseTableListApi = (params) =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryDiseaseList',
    params
  )
// 病害严重程度列表
export const diseaseStatusListApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryDiseaseLevelName'
  )
// 综合统计
export const compositeStatisticDataApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryGeneralStatistics'
  )
// 病害统计
export const diseaseStatisticDataApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryDiseaseStatistics'
  )
// 病害组成
export const diseaseCompositionDataApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryDiseaseCompose'
  )
// 异常跳车点(路线+路段)
export const jumpCarPointDataApi = () =>
  service.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/queryAbnormalJumps'
  )
// 导出接口(不进入请求拦截)
export const exportDiseaseDataApi = (params) =>
  axios.post(
    '/api/tlcloud/larkdata/visualcity/larkdata/visualcity/exportDiseaseList',
    params,
    { responseType: 'blob', headers: { token: getToken() } }
  )
