const TokenKey = 'Authorization_Rural_Screen_Plat'

export function getToken () {
  return sessionStorage.getItem(TokenKey)
}

export function setToken (token) {
  return sessionStorage.setItem(TokenKey, token)
}

export function removeToken () {
  return sessionStorage.removeItem(TokenKey)
}

const userName = TokenKey + '_Info'

export function getUserInfo () {
  return sessionStorage.getItem(userName)
}

export function setUserInfo (name) {
  return sessionStorage.setItem(userName, name)
}

export function removeUserInfo () {
  return sessionStorage.removeItem(userName)
}
