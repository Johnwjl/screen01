export const moveDown = function (e, self, collapseFlag) {
  const dialogDom = document.querySelector('.amap-info')
  // 初始化鼠标按下时,鼠标点在可视化区域的坐标
  let disX = e.clientX
  let disY = e.clientY
  //
  const currentLeft = dialogDom.offsetLeft
  const currentTop = dialogDom.offsetTop
  //
  const boxWidth = dialogDom.offsetWidth
  const boxHeight = dialogDom.offsetHeight
  //
  const screenWidth = document.querySelector('body').offsetWidth
  const screenHeight = document.querySelector('body').offsetHeight
  document.onmousemove = function (e) {
    let resLeft = currentLeft + (e.clientX - disX)
    let resTop = currentTop + (e.clientY - disY)
    if (collapseFlag) {
      // 左右边界
      if (resLeft < 0 + 250) { resLeft = 0 + 250 }
      if (resLeft > screenWidth - boxWidth + 250) {
        console.log('screenWidth', screenWidth)
        console.log('boxWidth', boxWidth)
        resLeft = screenWidth - boxWidth + 250
      }
      // 上下边界
      if (resTop < 100 + 100) { resTop = 100 + 100 }
      if (resTop > screenHeight - boxHeight + 100 - 50) { resTop = screenHeight - boxHeight + 100 - 50 }
    } else {
      // 左右边界
      if (resLeft < 0 + 250 + 300) { resLeft = 0 + 250 + 300 }
      if (resLeft > screenWidth - boxWidth + 250 - 300) {
        console.log('screenWidth', screenWidth)
        console.log('boxWidth', boxWidth)
        resLeft = screenWidth - boxWidth + 250 - 300
      }
      // 上下边界
      if (resTop < 100 + 100) { resTop = 100 + 100 }
      if (resTop > screenHeight - boxHeight + 100 - 50) { resTop = screenHeight - boxHeight + 100 - 50 }
    }
    // 移动当前元素
    dialogDom.style.left = `${resLeft}px`
    dialogDom.style.top = `${resTop}px`
    console.log(dialogDom.style.left, dialogDom.style.top)
  }
  document.onmouseup = function (e) {
    disX = null
    disY = null
    document.onmousemove = null
    document.onmouseup = null
  }
}
