import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './utils/rem'
import '../src/assets/font/iconfont.css'
import ElementPlus from 'element-plus'
import locale from 'element-plus/lib/locale/lang/zh-cn'
import 'element-plus/dist/index.css'
import '../src/assets/css/common.scss'

import moment from 'moment'
import 'moment/locale/zh-cn'
import bus from './utils/bus'
import echarts from 'echarts'
import MD5 from 'js-md5'

const app = createApp(App)
// 配置全局属性
app.config.globalProperties.$moment = moment
app.config.globalProperties.$echarts = echarts
app.config.globalProperties.$bus = bus
app.config.globalProperties.$md5 = MD5

app.use(ElementPlus, { locale })
app.use(store)
app.use(router)
app.mount('#app')
