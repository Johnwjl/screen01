module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/vue3-essential',
    '@vue/standard'
  ],
  parserOptions: {
    parser: 'babel-eslint'  
  },
  /**
   * "off" 或 0 - 关闭规则
   * "warn" 或 1 - 开启规则，使用警告级别的错误：warn (不会导致程序退出),
   * "error" 或 2 - 开启规则，使用错误级别的错误：error (当被触发的时候，程序会退出)
   */
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    // 禁止添加分号
    semi: [2, 'never'],
    // 缩进为2个空格
    indent: [2, 2],
    // 单引号
    quotes: [2, 'single'],
    // 禁用行尾空格
    'no-trailing-spaces': 2,
    // 要求使用 let 或 const 而不是 var
    'no-var': 0
  }
}
